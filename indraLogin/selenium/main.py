from selenium import webdriver
import time

from selenium.webdriver.common.keys import Keys

driver = webdriver.Chrome('chromedriver.exe')
#driver.get('https://login.indraweb.net/logon/LogonPoint/index.html#/dashboard/registro')

driver.get('https://apps.indraweb.net/registrohorario/#/dashboard/registro')

# Esperamos para que todos los elementos carguen y pueda localizar los identificadores en los tags
time.sleep(4)

# Se recoge cada uno de los elementos necesarios para hacer login
user_input = driver.find_element_by_id('login')
pass_input = driver.find_element_by_id('passwd')
submit_button = driver.find_element_by_id('nsg-x1-logon-button')

# Rellenando los diferentes campos con los valores necesarios. Colocar en user y passwd tus credenciales.
user = ""
passwd = ""

# Enviando los valores a los elementos
user_input.send_keys(user)
pass_input.send_keys(passwd)

# Enviamos la informacion del formulario
submit_button.click()

# Esperamos a que carguen los elementos
time.sleep(4)

# Boton de iniciar jornada
start_button = driver.find_element_by_class_name('c-button')

# Iniciamos la jornada
start_button.click()

# Vamos a comprobar si estamos ante un inicio o salida de jornada
start_time_value = ''

time.sleep(3)

# En la web de login, una vez se selecciona el inicio de jornada, la clase del elemento que muestra la hora pasa de
# unselected a selected, por lo tanto comprobando si existe algun tiempo selected, podemos saber si es el inicio o
# salida de jornada
try:
    end_button = driver.find_element_by_xpath("//button[contains(text(), 'Registrar jornada')]")
    end_button.click()
    print("La jornada ha finalizado correctamente")
except Exception as e:
    print("La jornada ha iniciado correctamente")
